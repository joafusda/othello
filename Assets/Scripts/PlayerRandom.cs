﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRandom : MonoBehaviour
{
    public int turn;
    private BoardManager boardManager;

    // Start is called before the first frame update
    void Start()
    {
        boardManager = GameObject.FindGameObjectWithTag("BoardManager").GetComponent<BoardManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
     * Entrada: Dado un tablero
     * Salida: Posición donde mueve  
     */
    public int SelectTile(Tile[] board)
    {
        List<int> selectableTiles = boardManager.FindSelectableTiles(board, turn);

        //Selecciono un movimiento aleatorio. Esto habrá que modificarlo para elegir el mejor movimiento según MINIMAX
        int movimiento = Random.Range(0, selectableTiles.Count);

        // Debug.Log("MOVIMIENTO RANDOM: " + movimiento);

        return selectableTiles[movimiento];
    }

}
