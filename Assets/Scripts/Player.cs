﻿/*********************************************************
 *        IA para Othello con algoritmo Minimax          *
 *                                                       *
 *        Joan Lluís Fuster Daviu                        *
 ********************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public Tile[] board = new Tile[Constants.NumTiles];
    public Node parent;
    public List<Node> childList = new List<Node>();
    public int type;  // Constants.MIN o Constants.MAX
    public int utility;
    public double alfa;
    public double beta;
    public int movimiento = -1;

    public Node(Tile[] tiles)
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            this.board[i] = new Tile();
            this.board[i].value = tiles[i].value;
        }
    }
}

public class Player : MonoBehaviour
{
    public int turn;
    private BoardManager boardManager;

    void Start()
    {
        boardManager = GameObject.FindGameObjectWithTag("BoardManager").GetComponent<BoardManager>();
    }

    /*
     * Entrada: Dado un tablero
     * Salida: Posición donde mueve  
     */
    public int SelectTile(Tile[] board)
    {
        // Generamos el nodo raíz del árbol (MAX)
        Node root = new Node(board);
        root.type = Constants.MAX;
        // Utilidad = -inf
        root.utility = -10000;

        // Llamada a minimax iterativo, profundidad 4
        root.utility = minimax(4, root, turn);

        // Selecciono el mejor movimiento según MINIMAX
        List<int> movimientos = new List<int>();
        foreach (Node node in root.childList)
        {
            if (node.utility == root.utility)
            {
                movimientos.Add(node.movimiento);
            }
        }
        // Para darle algo de aleatoriedad escoge uno de los de mayor utilidad
        int index = Random.Range(0, movimientos.Count);
        return movimientos[index];
    }

    private int minimax(int depth, Node nodo, int turno)
    {
        int bestScore;
        int currentScore;

        bestScore = nodo.utility;

        if (depth == 0)
        {
            // Calculamos la utilidad del nodo en el nivel de profundidad máxima
            nodo.utility = calcularUtilidad(nodo);
            bestScore = nodo.utility;
        }
        else
        {
            // Generamos el nivel de nodos hijos
            List<int> selectableTiles = boardManager.FindSelectableTiles(nodo.board, turno);

            // Si no hay jugadas (pasa o fin de partida)
            if (selectableTiles.Count == 0)
            {
                // Nodo hijo igual al nodo padre, con diferente type (MIN o MAX)
                Node n1 = new Node(nodo.board);
                // Lo añadimos a la lista de nodos hijo
                nodo.childList.Add(n1);
                // Enlazo con su padre
                n1.parent = nodo;
                // Si el padre es MAX, el hijo es MIN
                if (nodo.type == Constants.MAX)
                {
                    n1.type = Constants.MIN;
                    // Utilidad = +inf
                    n1.utility = 10000;

                    // Llamada al algoritmo iterativo con un nivel menos
                    currentScore = minimax(depth - 1, n1, -turno);

                    // Calcular mayor score
                    if (currentScore > bestScore)
                    {
                        bestScore = currentScore;
                        nodo.utility = bestScore;
                    }
                    // Comparar alfa con los beta superiores para saber si hay corte
                }
                // Si el padre es MIN, el hijo es MAX
                else
                {
                    n1.type = Constants.MAX;
                    // Utilidad = -inf
                    n1.utility = -10000;

                    // Llamada al algoritmo iterativo con un nivel menos
                    currentScore = minimax(depth - 1, n1, -turno);

                    // Calcular el menor score
                    if (currentScore < bestScore)
                    {
                        bestScore = currentScore;
                        nodo.utility = bestScore;
                    }
                }
            }
            // Hay jugadas disponibles
            else
            {
                foreach (int s in selectableTiles)
                {
                    // Creo un nuevo nodo hijo con el tablero padre
                    Node n1 = new Node(nodo.board);
                    // Guardo el movimiento que lleva a ese nodo
                    n1.movimiento = s;
                    // Lo añadimos a la lista de nodos hijo
                    nodo.childList.Add(n1);
                    // Enlazo con su padre
                    n1.parent = nodo;

                    // turno de la IA
                    if (nodo.type == Constants.MAX)
                    {
                        // Los hijos son MIN
                        n1.type = Constants.MIN;
                        // Utilidad = +inf
                        n1.utility = 10000;

                        // Aplico un movimiento, generando un nuevo tablero con ese movimiento
                        boardManager.Move(n1.board, s, turno);

                        // Llamada al algoritmo iterativo con un nivel menos
                        currentScore = minimax(depth - 1, n1, -turno);

                        // Calcular el mayor score
                        if (currentScore > bestScore)
                        {
                            bestScore = currentScore;
                            nodo.utility = currentScore;
                        }
                    }
                    // turno del oponente
                    else
                    {
                        // Los hijos son MAX
                        n1.type = Constants.MAX;
                        // Utilidad = -inf
                        n1.utility = -10000;

                        // Aplico un movimiento, generando un nuevo tablero con ese movimiento
                        boardManager.Move(n1.board, s, turno);

                        // Llamada al algoritmo iterativo con un nivel menos
                        currentScore = minimax(depth - 1, n1, -turno);

                        // Calcular el menor score
                        if (currentScore < bestScore)
                        {
                            bestScore = currentScore;
                            nodo.utility = bestScore;
                        }
                    }
                }
            }
        }
        return bestScore;
    }

    // Funcion que calcula la utilidad del nodo terminal
    private int calcularUtilidad(Node nodo)
    {
        // Cantidad de jugadas disponibles para la IA (mejor cuanto mayor sea)
        int jugadas = boardManager.FindSelectableTiles(nodo.board, turn).Count;
        // Casillas perdidas por la IA en el movimiento anterior al nodo terminal (mejor cuanto menor)
        int perdidas = boardManager.FindSwappablePieces(nodo.parent.board, nodo.movimiento, -turn).Count;
        // A la diferencia entre piezas de la IA y del contrario sumo o resto las anteriores con un coeficiente
        int utilidad = boardManager.CountPieces(nodo.board, turn) - boardManager.CountPieces(nodo.board, -turn) + 3*jugadas - 5*perdidas;
        return utilidad;
    }

}
